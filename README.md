# Brunch + Babel/ES6 + Sass/Autoprefixer + Materialize

To get started: `npm start`

To build for production: `npm run prod`

**Isues**

* Manual build times can be a bit slow (2s+), look into it.