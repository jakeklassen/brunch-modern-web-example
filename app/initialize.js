import { log } from './lib/logger';
import { default as $ } from 'jquery';

document.addEventListener('DOMContentLoaded', () => {
	// do your setup here
	log('Initialized app');
	log($ !== undefined ? 'jquery loaded' : 'failed to load jquery');
});
