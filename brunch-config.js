module.exports = {
	files: {
		javascripts: {
			joinTo: {
				'vendor.js': /^(?!app)/,
				'app.js': /^app/
			}
		},
		stylesheets: {
			joinTo: {
				'app.css': /^app/,
				'vendor.css': /^(bower_components|node_modules|vendor)/
			}
		}
	},

	plugins: {
		assetsmanager: {
			copyTo: {
				'fonts': ['node_modules/materialize-css/fonts/roboto']
			}
		},
		babel: {
			babelrc: true,
			ignore: [
				/^(node_modules|bower_components|vendor)/,
				'app/legacyES5Code/**/*'
			]
		},
		postcss: {
			processors: [
				require('autoprefixer')(['last 8 versions'])
			]
		},
		sass: {
			mode: 'native',
			options: {
				includePaths: ['node_modules/materialize-css/sass']
			}
		}
	}
};
